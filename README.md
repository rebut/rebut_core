### Rebut_core

Ce repository va contenir le code du robot de l'équipe Reb'UT. Notre vocation est de participer à la coupe de France de Robotique. Forts de 5 participations, nous avons du stopper nos activités durant une année. Nous sommes à nouveau en route pour cette belle aventure, et le travail commence dès maintenant ! 

Nous mettons ici en ligne le code servant à faire fonctionner notre robot. Dans un premier temps nous allons nous attacher à développer un simulateur permettant de travailler en amont de toute mécanique.